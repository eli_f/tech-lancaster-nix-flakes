#!/usr/bin/env bash
pandoc --embed-resources \
	--standalone \
	-t slidy \
	-s "$1.md" \
	-o "$1.html"

% nix flakes: dependency delight 
% Eli Flanagan
% September 28, 2023

## what would you call this?

<img src="what-is-it.jpg" title="What would you call this?" width="75%" />


## pick a picture

software is _

## pick a picture, nested

dependencies are _

## metaphors of pain?


::: incremental
- dependency hell
- DLL hell
- The Dependency Carousel (Dr. Dobbs article)
::: 

## let's make a sandbox on macOS

## install lima 

* <https://lima-vm.io/>

```
limactl start --name=tech-lancaster template://default
limactl shell tech-lancaster
```

## install configure nix 

This happens within the virtual machine.

```
sh <(curl -L https://nixos.org/nix/install) --no-daemon
mkdir -p ~/.config/nix
echo "experimental-features = nix-command flakes" >> ~/.config/nix/nix.conf
```

## three use cases


## example one: compiled tools
```
nix shell nixpkgs#poppler_utils
pdftotext -raw /tmp/PDF-Copy-of-New-Fee-Schedule-2023-1.pdf - | less
```

## example two: tools with complex runtimes

```
nix shell nixpkgs#jre17_minimal nixpkgs#maven
java -cp "$PWD/xsd2pgschema.jar" -jar xsd2pgschema.jar
```

::: incremental
- now laugh at my lack of java skills
:::

## example three: infosec fun

Let's securely send a file *with ease*.

- on machine A:
```
nix shell nixpkgs#magic-wormhole-rs
echo "beware worm-eaters" > /tmp/hello.txt
wormhole-rs send /tmp/hello.txt
```
- on machine B:
```
nix shell nixpkgs#magic-wormhole-rs
wormhole-rs receive INSERT_CODE_HERE
```

## links for learning

- <https://nix.dev>
- <https://zero-to-nix.com/>
- <https://lima-vm.io/>

- what is this word poppler? 
    - a tasty snack humans gobbled billions of only to realize the snacks speak. Futurama: <https://web.archive.org/web/20230205142122/https://www.gotfuturama.com/Information/Encyc-41-Popplers/>

## addenda
- <https://multipass.run/docs/mac-tutorial> Ubuntu specific cross-platform virtualization option. Thanks Matt for the knowledge!
